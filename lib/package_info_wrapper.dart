import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';

class PackageInfoWrapper {
  PackageInfoWrapper({
    this.appName,
    this.packageName,
    this.version,
    this.buildNumber,
  });

  static PackageInfoWrapper _fromPlatform;

  static Future<PackageInfoWrapper> fromPlatform() async {
    if (_fromPlatform != null) {
      return _fromPlatform;
    }

    if (kIsWeb) {
      _fromPlatform = _fromPlatform = PackageInfoWrapper(
        appName: "DummyAppName",
        packageName: "DummyPakcageName",
        version: "1.0.0-dummy",
        buildNumber: 001000000,
      );
    } else {
      final packageInfo = await PackageInfo.fromPlatform();
      _fromPlatform = _fromPlatform = PackageInfoWrapper(
        appName: packageInfo.appName,
        packageName: packageInfo.packageName,
        version: packageInfo.version,
        buildNumber: int.parse(packageInfo.buildNumber),
      );
    }
    return _fromPlatform;
  }

  final String appName;

  final String packageName;

  final String version;

  final int buildNumber;
}
